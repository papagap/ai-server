package apostolos.dlserver;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import apostolos.dlserver.DeeplearningServerApplication;
import apostolos.dlserver.conf.ConfigurationService;
import apostolos.dlserver.model.DnnPattern;
import apostolos.dlserver.model.DnnTuple;
import apostolos.dlserver.model.FeaturesRecord;
import apostolos.dlserver.repository.FeaturesRecordRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DeeplearningServerApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = ConfigurationService.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DlserverApplicationTests {
	
	@LocalServerPort
	private int port;
	
	@Autowired
    private TestRestTemplate restTemplate;

	@Autowired
    private FeaturesRecordRepository featuresRecordRepo;
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void test1_AddFeaturesRecords_CorrectInputs_ShouldSucceed() throws URISyntaxException  {
		
		int maxNumOfFeaturesInRecord = 10;
		int numOfRecordsToCreate = 2000;
		
		FeaturesRecord fr;
		HashMap<String,Double> features;
		Random rand = new Random();
		
		long startNumOfFeatureRecordsInDb = featuresRecordRepo.count();
		
		for (int i=0; i<numOfRecordsToCreate; i++) {
			fr = new FeaturesRecord();
			features = new HashMap<String,Double>();
			int numOfFeaturesInRecord = rand.nextInt(maxNumOfFeaturesInRecord) + 1;
			for (int j=0; j<numOfFeaturesInRecord; j++) {
				features.put("x"+j, rand.nextDouble());
			}
			fr.setFeatures(features);
			
			//featuresRecordRepo.save(fr); Replaced with POST invocation below, to test the Web Layer as well
			final String baseUrl = "http://localhost:"+ port +"/api/v1/featuresRecord/";
	        URI uri = new URI(baseUrl);
	        HttpHeaders headers = new HttpHeaders();
	        headers.set("X-COM-PERSIST", "true");      	 
	        HttpEntity<FeaturesRecord> request = new HttpEntity<>(fr, headers);
	        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
	        Assert.assertEquals(200, result.getStatusCodeValue());
		}
		
		long endNumOfFeatureRecordsInDb = featuresRecordRepo.count();
		
		Assert.assertEquals(endNumOfFeatureRecordsInDb, startNumOfFeatureRecordsInDb + numOfRecordsToCreate);
	}
	
	@Test
	public void test2_AddDnnPatternAndTuples_CorrectInputs_ShouldSucceed() throws URISyntaxException {
		
		int numOfTuplesToCreate = 20;
		
		DnnPattern dp = new DnnPattern();
		dp.setName("TestDnnPattern");
		dp.setOutput("y");
		dp.setUtilityFunction("Test uf"); // TODO: Doesn't matter yet. Play with it as soon as related logic is implemented.
		dp.setThreshold(0.5); // TODO: Doesn't matter yet. Play with it as soon as related logic is implemented.
		String[] validOutputClasses = {"0", "1"};
		dp.setValidOutputClasses(validOutputClasses); // TODO: Doesn't matter yet. Play with it as soon as related logic is implemented.
		
		ArrayList<String> inputs = new ArrayList<String>();
		inputs.add("x1");
		inputs.add("x4");
		inputs.add("x5");
		inputs.add("x7");
		
		dp.setInputs(inputs);
		
		final String baseUrl = "http://localhost:"+ port +"/api/v1/dnnPattern/";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");
        HttpEntity<DnnPattern> request = new HttpEntity<>(dp, headers);
        ResponseEntity<DnnPattern> result = this.restTemplate.postForEntity(uri, request, DnnPattern.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
        dp = result.getBody();
        
        for (int i=0; i<numOfTuplesToCreate; i++) {
        	addDnnTupleForDnn_RandomInputs_ShouldPrintOutputs(dp);
        }
	}
	
	public void addDnnTupleForDnn_RandomInputs_ShouldPrintOutputs(DnnPattern dp) throws URISyntaxException {
		DnnTuple dt = new DnnTuple();
		dt.setPatternId(dp.getId());
		
		dt.setTrainingItem(false);
		
		HashMap<String,Double> inputValues = new HashMap<String,Double>();
		Random r = new Random();
		
		for (int i=0; i<dp.getInputs().size(); i++) {
			String currFeatureName = dp.getInputs().get(i);
			double val = r.nextDouble();
			inputValues.put(currFeatureName, val);
		}
		
		dt.setInputValues(inputValues);
		
		final String baseUrl = "http://localhost:"+ port +"/api/v1/dnnTuple/";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");
        HttpEntity<DnnTuple> request = new HttpEntity<>(dt, headers);
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
        Assert.assertEquals(200, result.getStatusCodeValue());
	}

}
