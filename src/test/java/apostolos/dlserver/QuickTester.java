package apostolos.dlserver;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import apostolos.dlserver.DeeplearningServerApplication;
import apostolos.dlserver.Util;
import apostolos.dlserver.conf.ConfigurationService;
import apostolos.dlserver.model.DnnPattern;
import apostolos.dlserver.model.DnnTuple;
import apostolos.dlserver.model.FeaturesRecord;
import apostolos.dlserver.repository.DnnTupleRepository;
import apostolos.dlserver.repository.FeaturesRecordRepository;

import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DeeplearningServerApplication.class)
@ContextConfiguration(classes = ConfigurationService.class)
public class QuickTester {
	
	@Autowired
	private ConfigurationService configService;
	
	@Autowired
    private FeaturesRecordRepository featuresRecordRepo;
	
	@Autowired
    private DnnTupleRepository dnnTupleRepo;

	@Test
	public void datasetPreparationCsvTest() {
		
		DnnPattern dp = new DnnPattern();
		dp.setName("TestDnnPattern");
		dp.setOutput("y");
		dp.setUtilityFunction("Test uf");
		dp.setThreshold(0.5);
		String[] validOutputClasses = {"0", "1"};
		dp.setValidOutputClasses(validOutputClasses);
		
		ArrayList<String> inputs = new ArrayList<String>();
		inputs.add("x1");
		inputs.add("x4");
		inputs.add("x5");
		inputs.add("x7");
		
		dp.setInputs(inputs);
		
		ArrayList<FeaturesRecord> allFeaturesRecords = (ArrayList<FeaturesRecord>) featuresRecordRepo.findAll();
		ArrayList<DnnTuple> generatedDnnTuples = Util.generateTuplesFromRecordsForPattern(allFeaturesRecords, dp);
		
		System.out.println("NumOfFeaturesRec: " + allFeaturesRecords.size());
		System.out.println("NumOfDnnTuples: " + generatedDnnTuples.size());
		
		dnnTupleRepo.saveAll(generatedDnnTuples);
		
		Assert.assertNotNull(configService.getMongoConfig().getHost());
		
		/*
		DnnPattern dp = new DnnPattern();
		dp.setId("string");
		ArrayList<String> inputs = new ArrayList<String>();
		inputs.add("x");
		inputs.add("y");
		dp.setInputs(inputs);
		
		Util.convertDnnTupleMongoEntriesToCSV(dp, configService.getMongoConfig());
		
		Assert.assertNotNull(configService.getMongoConfig().getHost());
		*/
	}
	
	/*
	public static void main(String[] args) {
		
		DnnPattern dp = new DnnPattern();
		ArrayList<String> inputs = new ArrayList<String>();
		inputs.add("x");
		inputs.add("y");
		dp.setInputs(inputs);
		
		Util.convertDnnTupleMongoEntriesToCSV(dp);

        String[] inputs = {"x","y"};

        String command = "mongoexport.exe --host " + Configuration.DB_HOST + " --port " + Configuration.DB_PORT + " --db " + Configuration.DB_NAME + " --collection " + Configuration.DB_DNN_TUPLE_COLLECTION_NAME + " --type=csv --noHeaderLine --fields inputValues." + inputs[0] + ",inputValues." + inputs[1] + ",outputValue --out " + Configuration.CSV_EXPORTED_FEATURES_FILENAME + "";

        try {
            System.out.println(command);
            Process process = Runtime.getRuntime().exec(command);
            int waitFor = process.waitFor();
            System.out.println("waitFor:: " + waitFor);
            BufferedReader success = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String s = "";
            while ((s = success.readLine()) != null) {
                System.out.println(s);
            }

            while ((s = error.readLine()) != null) {
                System.out.println("Std ERROR : " + s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }*/
}
