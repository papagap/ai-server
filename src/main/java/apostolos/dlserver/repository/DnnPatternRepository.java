package apostolos.dlserver.repository;

//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import apostolos.dlserver.model.DnnPattern;

@Repository
public interface DnnPatternRepository extends MongoRepository<DnnPattern, String> {
}