package apostolos.dlserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import apostolos.dlserver.model.FeaturesRecord;

@Repository
public interface FeaturesRecordRepository extends MongoRepository<FeaturesRecord, String> {
}