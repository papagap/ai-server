package apostolos.dlserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import apostolos.dlserver.model.DnnTuple;

@Repository
public interface DnnTupleRepository extends MongoRepository<DnnTuple, String> {
}