package apostolos.dlserver.repository;

import java.util.HashMap;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class DnnInstanceRepository {
	
	// TODO: Consider making this a Spring MongoDB repository like the other repositories in order to be able to persist DNN instances.
	// However, persisting a multi-layer network (which is generated, trained, and potentially updated at runtime) might be challenging.
	private HashMap<String,MultiLayerNetwork> instances = new HashMap<String,MultiLayerNetwork>();

}
