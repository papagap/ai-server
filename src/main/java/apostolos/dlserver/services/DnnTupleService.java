package apostolos.dlserver.services;

import java.util.NoSuchElementException;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import apostolos.dlserver.model.DnnPattern;
import apostolos.dlserver.model.DnnTuple;
import apostolos.dlserver.repository.DnnInstanceRepository;
import apostolos.dlserver.repository.DnnPatternRepository;

@Service
public class DnnTupleService {
	
	protected static Logger log = LoggerFactory.getLogger(DnnTupleService.class);
	
	@Autowired
    private DnnInstanceRepository dnnInstanceRepo;
	
	@Autowired
    private DnnPatternRepository dnnPatternRepo;
	
	public DnnTuple inferOutputValues(DnnTuple dt) {
		
		if (dt == null || dt.getInputValues() == null || dt.getPatternId() == null) {
			log.error("The provided DNN tuple was null or had null input values or pattern id, so no infrence can be done.");
			return dt;
		}
		
		if (!dt.isTrainingItem()) {
			
			DnnPattern relatedPattern = null;
			try {
				relatedPattern = dnnPatternRepo.findById(dt.getPatternId()).get();
				if (relatedPattern.getInputs() == null || relatedPattern.getInputs().size() == 0) {
					log.error("The DNN pattern with id = " + dt.getPatternId() + " has no defined inputs, so no inference can be done.");
				}
			} catch (NoSuchElementException nsee) {
				log.error("No DNN pattern exists with id = " + dt.getPatternId());
			}
			
			MultiLayerNetwork dnnInstance = null;
			if (dnnInstanceRepo.getInstances() != null) {
				dnnInstance = dnnInstanceRepo.getInstances().get(dt.getPatternId());
			} else {
				log.error("No DNN instance exists for pattern id " + dt.getPatternId());
			}
			
			if (relatedPattern == null || relatedPattern.getInputs() == null || dnnInstance == null) {
				return dt;
			}
			
			double[] tupleInputsAsArray = new double[relatedPattern.getInputs().size()];
			for (int i=0; i<tupleInputsAsArray.length; i++) {
				String currFeatureName = relatedPattern.getInputs().get(i);
				if (currFeatureName == null) {
					log.error("One of the inputs of the DNN pattern was null, so no inference can be done.");
					return dt;
				}
				Double currFeatureVal = dt.getInputValues().get(currFeatureName);
				if (currFeatureVal == null) {
					log.error("One of the input values of the DNN tuple was null, so no inference can be done.");
					return dt;
				}
				tupleInputsAsArray[i] = currFeatureVal; // TODO: Examine if data normalization is needed or the inputs will be required to be provided normalized (double from 0 to 1?) anyway.
			}
			
			//INDArray inputRow = Nd4j.rand(1,4);
			INDArray inputRow = Nd4j.create(tupleInputsAsArray);
			log.debug("\n");
			log.debug("Input feature vector: " + inputRow.toString());
			INDArray output = dnnInstance.output(inputRow); // TODO: If the exact computed probabilities for each output class are not to be used, remove this and keep only the "predict" call for performance reasons
			log.debug("Output label probabilities: " + output.toString());
			int[] predicted = dnnInstance.predict(inputRow);
			log.debug("Predicted output class: " + predicted[0]); // Note that the array has only one element, i.e., predicted[1] throws OutOfBounds. As you can see at https://deeplearning4j.org/tutorials/01-multilayernetwork-and-computationgraph, this is always the case for a MultiLayerNetwork. In order to have multiple outputs, the ComputationGraph must be used instead.
			
			dt.setOutputValue(predicted[0]);
		}
		
		return dt;
	}

}
