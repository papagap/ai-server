package apostolos.dlserver.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import apostolos.dlserver.Util;
import apostolos.dlserver.conf.MongoConfiguration;
import apostolos.dlserver.model.DnnPattern;
import apostolos.dlserver.model.DnnTuple;
import apostolos.dlserver.model.FeaturesRecord;
import apostolos.dlserver.repository.DnnInstanceRepository;
import apostolos.dlserver.repository.DnnTupleRepository;
import apostolos.dlserver.repository.FeaturesRecordRepository;

@Service
public class DnnPatternService {
	
	protected static Logger log = LoggerFactory.getLogger(DnnPatternService.class);
	
	@Autowired
	private MongoConfiguration mongoConfig;
	
	@Autowired
	private FeaturesRecordRepository featuresRecordRepo;
	
	@Autowired
    private DnnTupleRepository dnnTupleRepo;
	
	@Autowired
    private DnnInstanceRepository dnnInstanceRepo;
	
	public void generateDnn(DnnPattern dp) {
		MultiLayerConfiguration configuration = new NeuralNetConfiguration.Builder()
			.iterations(1000) // TODO: Make this a field in the DnnPattern, a constant in this class, or a property? Same for all rows below...
			.activation(Activation.TANH)
            .weightInit(WeightInit.XAVIER)
            .regularization(true)
            .learningRate(0.1).l2(0.0001)
            .list()
            .layer(0, new DenseLayer.Builder()
            	.nIn(dp.getInputs().size())
            	.nOut(3) // TODO: This can be almost anything, but must be the same with the nIn of the next Layer
            	.build())
            .layer(1, new DenseLayer.Builder()
            	.nIn(3)
            	.nOut(3) 
                .build())
            .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                .activation(Activation.SOFTMAX)
                .nIn(3)
                .nOut(dp.getValidOutputClasses().length) // This must be the number of possible classes of the output. See https://www.baeldung.com/deeplearning4j for hints about all these numbers.
                .build())
            .backpropType(BackpropType.Standard).pretrain(false)
            .build();

        MultiLayerNetwork model = new MultiLayerNetwork(configuration);
        
        model.init();
        
        ArrayList<FeaturesRecord> allFeaturesRecords = (ArrayList<FeaturesRecord>) featuresRecordRepo.findAll();
        ArrayList<DnnTuple> generatedDnnTuples = Util.generateTuplesFromRecordsForPattern(allFeaturesRecords, dp);
        ArrayList<DnnTuple> savedDnnTuples = (ArrayList<DnnTuple>) dnnTupleRepo.saveAll(generatedDnnTuples);
        log.debug("NumOfFeaturesRec: " + allFeaturesRecords.size());
        log.debug("NumOfGeneratedDnnTuples: " + generatedDnnTuples.size());
        log.debug("NumOfSavedDnnTuples: " + savedDnnTuples.size());
        int datasetLength = Util.convertDnnTupleMongoEntriesToCSV(dp, mongoConfig);
        int outputColumnIndex = dp.getInputs().size(); // This is just the index of the class column, which happens to be the same like that length, because we'll use the last column for the output
        
        DataSet data = new DataSet();
        try (RecordReader recordReader = new CSVRecordReader(0, ',')) {
        	File csvFile = new File(mongoConfig.getCsvExportedFeaturesFilename());
        	recordReader.initialize(new FileSplit(csvFile));
        	DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, datasetLength, outputColumnIndex, dp.getValidOutputClasses().length);
        	data = iterator.next();
    	} catch (IOException ioe) {
    		log.error("IO failure while reading dataset from CSV file: " + ioe.getMessage());
    	} catch (InterruptedException ie) {
    		log.error("Interuption-caused failure while reading dataset from CSV file: " + ie.getMessage());
    	}
    	DataNormalization normalizer = new NormalizerStandardize(); // TODO: An Exception is thrown because of the lines of code that follow. Check and fix. 
    	normalizer.fit(data);
    	normalizer.transform(data);
    	
        model.fit(data); // This actually performs the training step. The model should be afterwards usable for getting features as input and inferring an output, but this should be done when POSTing a new DnnTuple which is meant for inference and not for training.
        
        dnnInstanceRepo.getInstances().put(dp.getId(), model);
	}

}
