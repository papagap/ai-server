package apostolos.dlserver.model;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;

//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

// TODO: Choose a data storage approach.
// The commented-out parts are for JPA/SQL-based solutions.
// However, it is usually required to change more than just the annotations when switching from NoSQL to relational.
// For example, the fields of type "ArrayList" should be replaced by references/foreign-keys to separate models,
// either by using the ElementCollection (see https://www.callicoder.com/hibernate-spring-boot-jpa-element-collection-demo/)
// or by using the one-to-many mapping (see https://www.callicoder.com/hibernate-spring-boot-jpa-one-to-many-mapping-example/).
// Similar differences would exist in other cases, because the NoSQL and relational approaches are meant for different scenarios or kinds of data.
// It is therefore not really sufficient to change only the repository when switching among major data storage approaches.
// The Spring Data project (https://spring.io/projects/spring-data) is an attempt to introduce another abstraction layer
// in the direction of programming really repo-independently, but it is not further examined here.  

@Data
//@Entity
//@Table(name = "dnn_pattern")
@ApiModel(description = "The object that represents a Deep Neural Network pattern, i.e., the information required to generate a new DNN with specific characteristics")
public class DnnPattern {
	
	// TODO: Update model to better reflect the model of dl4j's MultiLayerNetwork? Currently many DNN characteristics are "hardcoded" in the DnnPatternService.
	
    @ApiModelProperty(notes = "The database-generated dnn pattern ID")
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    //@Column(name = "id", nullable = false)
    private String id;
    
    @ApiModelProperty(notes = "The dnn name")
    //@Column(name = "name", nullable = false)
    private String name;
    
    @ApiModelProperty(notes = "The list of names of the DNN input parameters, as these names are stored in the repository")
    //@Column(name = "inputs", nullable = false)
    private ArrayList<String> inputs;
    
    @ApiModelProperty(notes = "The name of the DNN output parameter, as this name is stored in the repository. Note that only single-output DNNs are considered here.")
    //@Column(name = "output", nullable = false)
    private String output;
    
    @ApiModelProperty(notes = "A list of the possible classes for the output of this DNN, whereby the index of a class in this list shall correspond with the integer value used in the data set to represent this class.")
    //@Column(name = "validOutputClasses", nullable = false)
    private String[] validOutputClasses;
    
    @ApiModelProperty(notes = "A utility function that depends on parameters that will not be used in the DNN neither as inputs nor as outputs, but they will be used to decide which (output) class each tuple of this DNN belongs to.")
    //@Column(name = "utility_function")
    private String utilityFunction;
    
    @ApiModelProperty(notes = "A threshold which will determine the (output) class of the DNN tuples. Records for which the utility function is above this threshold shall be converted to DNN tuples with outputValue equal to 1 (otherwise it should be 0).")
    //@Column(name = "threshold")
    private double threshold;
}
