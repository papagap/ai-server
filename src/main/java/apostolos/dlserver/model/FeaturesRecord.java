package apostolos.dlserver.model;

import java.util.Date;
import java.util.HashMap;

import org.springframework.data.annotation.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;  

@Data
@ApiModel(description = "The object that represents a features record, namely a map of key-value pairs and their timestamp (without any classification/output or other DNN-specific feature)")
public class FeaturesRecord {
	
    @ApiModelProperty(notes = "The database-generated features record ID")
    @Id
    private String id;
    
    @ApiModelProperty(notes = "The timestamp of this record")
    private Date timestamp; // TODO: Needed or stored by the mongo db repository automatically? (see https://stackoverflow.com/questions/27895955/how-to-save-timestamp-type-value-in-mongodb-java)
    
    @ApiModelProperty(notes = "A map with the names of the features and their respective values.")
    private HashMap<String,Double> features;
}
