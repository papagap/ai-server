package apostolos.dlserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeeplearningServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeeplearningServerApplication.class, args);
	}

}
