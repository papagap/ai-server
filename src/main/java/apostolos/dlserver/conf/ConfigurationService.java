package apostolos.dlserver.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
public class ConfigurationService {
	
    @Autowired
    private MongoConfiguration mongoConfig;

    public MongoConfiguration getMongoConfig() {
        return mongoConfig;
    }

    public void showConfig() {
        mongoConfig.toString();
    }
}
