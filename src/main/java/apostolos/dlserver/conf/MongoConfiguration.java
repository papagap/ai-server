package apostolos.dlserver.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "spring.data.mongodb")
@Data
public class MongoConfiguration {
	
	private String database;
	private String host;
	private String port;
	private String dnnPatternCollectionName;
	private String dnnTupleCollectionName;
	private String csvExportedFeaturesFilename;

}
