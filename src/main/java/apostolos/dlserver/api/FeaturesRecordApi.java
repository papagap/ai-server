package apostolos.dlserver.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import apostolos.dlserver.errors.ResourceNotFoundException;
import apostolos.dlserver.model.FeaturesRecord;
import apostolos.dlserver.repository.FeaturesRecordRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Features Record Management Endpoint")
public class FeaturesRecordApi {
	
    @Autowired
    private FeaturesRecordRepository featuresRecordRepo;
    
    @ApiOperation(value = "View a list of stored features records", response = List.class)
    @GetMapping("/featuresRecord")
    public List<FeaturesRecord> getAllFeaturesRecords() {
        return featuresRecordRepo.findAll();
    }
    
    @ApiOperation(value = "Get a features record by ID")
    @GetMapping("/featuresRecord/{id}")
    public ResponseEntity<FeaturesRecord> getFeaturesRecordById(
    		@ApiParam(value = "FeaturesRecord id for which the FeaturesRecord object shall be retrieved", required = true) @PathVariable(value = "id") String featuresRecordId) throws ResourceNotFoundException {
    	FeaturesRecord featuresRecord = featuresRecordRepo.findById(featuresRecordId).orElseThrow(() -> new ResourceNotFoundException("Features record not found for this id: " + featuresRecordId));
        return ResponseEntity.ok().body(featuresRecord);
    }
    
    @ApiOperation(value = "Add a features record")
    @PostMapping("/featuresRecord")
    public FeaturesRecord createFeaturesRecord(
    		@ApiParam(value = "FeaturesRecord object to be stored in the repository", required = true) @Valid @RequestBody FeaturesRecord featuresRecord) {
    	return featuresRecordRepo.save(featuresRecord);
    }
    
    @ApiOperation(value = "Delete a features record")
    @DeleteMapping("/featuresRecord/{id}")
    public Map<String,Boolean> deleteFeaturesRecord(
    		@ApiParam(value = "Features record id for which the FeaturesRecord object shall be deleted", required = true) @PathVariable(value = "id") String featuresRecordId) throws ResourceNotFoundException {
    	FeaturesRecord featuresRecord = featuresRecordRepo.findById(featuresRecordId).orElseThrow(() -> new ResourceNotFoundException("Features record not found for this id: " + featuresRecordId));
    	featuresRecordRepo.delete(featuresRecord);
        Map<String,Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
}