package apostolos.dlserver.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import apostolos.dlserver.errors.ResourceNotFoundException;
import apostolos.dlserver.model.DnnTuple;
import apostolos.dlserver.repository.DnnTupleRepository;
import apostolos.dlserver.services.DnnTupleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/v1")
@Api(value = "DNN Tuple Management Endpoint")
public class DnnTupleApi {
	
	protected static Logger log = LoggerFactory.getLogger(DnnTupleApi.class);
	
    @Autowired
    private DnnTupleRepository dnnTupleRepo;
    
    @Autowired
    private DnnTupleService dnnTupleService;
    
    @ApiOperation(value = "View a list of stored DNN tuples", response = List.class)
    @GetMapping("/dnnTuple")
    public List<DnnTuple> getAllDnnTuples() {
        return dnnTupleRepo.findAll();
    }
    
    @ApiOperation(value = "Get a DNN tuple by ID")
    @GetMapping("/dnnTuple/{id}")
    public ResponseEntity<DnnTuple> getDnnTupleById(
    		@ApiParam(value = "DNN tuple id for which the DnnTuple object shall be retrieved", required = true) @PathVariable(value = "id") String dnnTupleId) throws ResourceNotFoundException {
    	DnnTuple dnnTuple = dnnTupleRepo.findById(dnnTupleId).orElseThrow(() -> new ResourceNotFoundException("DNN tuple not found for this id: " + dnnTupleId));
        return ResponseEntity.ok().body(dnnTuple);
    }
    
    @ApiOperation(value = "Add a DNN tuple")
    @PostMapping("/dnnTuple")
    public DnnTuple createDnnTuple(
    		@ApiParam(value = "DNN tuple object to be stored in the repository", required = true) @Valid @RequestBody DnnTuple dnnTuple) {
    	DnnTuple inferedDnnTuple = new DnnTuple();
    	inferedDnnTuple = dnnTupleService.inferOutputValues(dnnTuple);
    	return dnnTupleRepo.save(inferedDnnTuple);
    }
    
    @ApiOperation(value = "Delete a DNN tuple")
    @DeleteMapping("/dnnTuple/{id}")
    public Map<String,Boolean> deleteDnnTuple(
    		@ApiParam(value = "DNN tuple id for which the DnnTuple object shall be deleted", required = true) @PathVariable(value = "id") String dnnTupleId) throws ResourceNotFoundException {
    	DnnTuple dnnTuple = dnnTupleRepo.findById(dnnTupleId).orElseThrow(() -> new ResourceNotFoundException("DNN tuple not found for this id: " + dnnTupleId));
        dnnTupleRepo.delete(dnnTuple);
        Map<String,Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
}