package apostolos.dlserver.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import apostolos.dlserver.errors.ResourceNotFoundException;
import apostolos.dlserver.model.DnnPattern;
import apostolos.dlserver.repository.DnnPatternRepository;
import apostolos.dlserver.services.DnnPatternService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/v1")
@Api(value = "DNN Pattern Management Endpoint")
public class DnnPatternApi {
	
    @Autowired
    private DnnPatternRepository dnnPatternRepo;
    
    @Autowired
    private DnnPatternService dnnPatternService;
    
    @ApiOperation(value = "View a list of available DNN patterns", response = List.class)
    /* The code below can be used for customizing the messages for specific codes.
     * The codes 200, 401, 403, 404 are defined in swagger for GET operations anyway (with the default responses) even without using the ApiResponses annotation.
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })*/
    @GetMapping("/dnnPattern")
    public List<DnnPattern> getAllDnnPatterns() {
        return dnnPatternRepo.findAll();
    }
    
    @ApiOperation(value = "Get a DNN pattern by ID")
    @GetMapping("/dnnPattern/{id}")
    public ResponseEntity<DnnPattern> getDnnPatternById(
    		@ApiParam(value = "DNN Pattern id for which the DnnPattern object shall be retrieved", required = true) @PathVariable(value = "id") String dnnPatternId) throws ResourceNotFoundException {
    	DnnPattern dnnPattern = dnnPatternRepo.findById(dnnPatternId).orElseThrow(() -> new ResourceNotFoundException("DNN pattern not found for this id: " + dnnPatternId));
        return ResponseEntity.ok().body(dnnPattern);
    }
    
    @ApiOperation(value = "Add a DNN Pattern")
    @PostMapping("/dnnPattern")
    public DnnPattern createDnnPattern(
    		@ApiParam(value = "DNN pattern object to be stored in the repository", required = true) @Valid @RequestBody DnnPattern dnnPattern) {
    	dnnPattern = dnnPatternRepo.save(dnnPattern);
    	dnnPatternService.generateDnn(dnnPattern);
    	return dnnPattern;
    }
    
    @ApiOperation(value = "Update a DNN pattern")
    @PutMapping("/dnnPattern/{id}")
    public ResponseEntity<DnnPattern> updateDnnPattern(
    		@ApiParam(value = "DNN Pattern id for which the DnnPattern object shall be updated", required = true) @PathVariable(value = "id") String dnnPatternId,
    		@ApiParam(value = "Updated DNN pattern object to be stored in the repository", required = true) @Valid @RequestBody DnnPattern newDnnPattern) throws ResourceNotFoundException {
    	DnnPattern dnnPattern = dnnPatternRepo.findById(dnnPatternId).orElseThrow(() -> new ResourceNotFoundException("DNN pattern not found for this id: " + dnnPatternId));
        dnnPattern.setName(newDnnPattern.getName());
        dnnPattern.setInputs(newDnnPattern.getInputs());
        dnnPattern.setOutput(newDnnPattern.getOutput());
        dnnPattern.setValidOutputClasses(newDnnPattern.getValidOutputClasses());
        dnnPattern.setUtilityFunction(newDnnPattern.getUtilityFunction());
        dnnPattern.setThreshold(newDnnPattern.getThreshold());
        final DnnPattern updatedDnnPattern = dnnPatternRepo.save(dnnPattern);
        return ResponseEntity.ok(updatedDnnPattern);
    }
    
    @ApiOperation(value = "Delete a DNN pattern")
    @DeleteMapping("/dnnPattern/{id}")
    public Map<String,Boolean> deleteDnnPattern(
    		@ApiParam(value = "DNN Pattern id for which the DnnPattern object shall be deleted", required = true) @PathVariable(value = "id") String dnnPatternId) throws ResourceNotFoundException {
    	DnnPattern dnnPattern = dnnPatternRepo.findById(dnnPatternId).orElseThrow(() -> new ResourceNotFoundException("DNN pattern not found for this id: " + dnnPatternId));
        dnnPatternRepo.delete(dnnPattern);
        Map<String,Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
}