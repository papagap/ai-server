package apostolos.dlserver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import apostolos.dlserver.conf.MongoConfiguration;
import apostolos.dlserver.model.DnnPattern;
import apostolos.dlserver.model.DnnTuple;
import apostolos.dlserver.model.FeaturesRecord;

public class Util {
	
	protected static Logger log = LoggerFactory.getLogger(Util.class);
	
	public static int convertDnnTupleMongoEntriesToCSV(DnnPattern dp, MongoConfiguration mongoConf) {
        ArrayList<String> inputs = dp.getInputs();
        String patternIdQuery = "\"{patternId: \'" + dp.getId() +"\'}\"";

        String command = "mongoexport.exe --host " + mongoConf.getHost() + " --port " + mongoConf.getPort() + " --db " + mongoConf.getDatabase() + " --collection " + mongoConf.getDnnTupleCollectionName() + " --query " + patternIdQuery + " --type=csv --noHeaderLine --fields ";
        for (String input : inputs) {
        	command += "inputValues." + input + ",";
        }
        command += "outputValue --out " + mongoConf.getCsvExportedFeaturesFilename() + "";
        
        try {
        	log.debug("Command for CSV generation: " + command);
            Process process = Runtime.getRuntime().exec(command);
            int waitFor = process.waitFor();
            log.debug("waitFor:: " + waitFor);
            BufferedReader success = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String s = "";
            while ((s = success.readLine()) != null) {
                log.debug(s);
            }

            while ((s = error.readLine()) != null) {
            	log.debug("Std error line: " + s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        int csvFileLength = getNumberOfLinesOfFile(mongoConf.getCsvExportedFeaturesFilename());
        
        return csvFileLength;
	}
	
	public static int getNumberOfLinesOfFile(String filename) {
		int countOfLines = 0;
		try {
			FileReader fr = new FileReader(filename);
			LineNumberReader lnr = new LineNumberReader(fr); 
			while (lnr.readLine() != null) {
				countOfLines++;
			}
			lnr.close();
		} catch (FileNotFoundException fnfe) {
			log.error("Failure while trying to open file to count its lines: " + fnfe.getMessage());
		} catch (IOException ioe) {
			log.error("Failure counting the number of lines of a file: " + ioe.getMessage());
		} 
	    return countOfLines;
	}
	
	public static ArrayList<DnnTuple> generateTuplesFromRecordsForPattern(ArrayList<FeaturesRecord> recordList, DnnPattern dp) {
		ArrayList<DnnTuple> tupleList = new ArrayList<DnnTuple>();
		for (FeaturesRecord record : recordList) {
			DnnTuple tuple = new DnnTuple();
			tuple.setPatternId(dp.getId());
			tuple.setTrainingItem(true);
			HashMap<String, Double> inputValues = new HashMap<String, Double>();
			boolean recordHasAllRequiredFeatures = true;
			for (String input : dp.getInputs()) {
				if (record.getFeatures().get(input) != null) {  // TODO: Try to convert and use also records that did not have all the tuple-required inputs inside?
					inputValues.put(input, record.getFeatures().get(input));
				} else {
					recordHasAllRequiredFeatures = false;
					break;
				}
			}
			if (recordHasAllRequiredFeatures) {
				tuple.setInputValues(inputValues);
				int recordClass = assignClass(record, dp.getUtilityFunction(), dp.getThreshold());
				tuple.setOutputValue(recordClass);
				tupleList.add(tuple);
			}
		}
		return tupleList;
	}

	private static int assignClass(FeaturesRecord record, String utilityFunction, double threshold) {
		// TODO At the moment -just for testing- it ignores the utility function and the threshold and "tends" to assign tuples with high values to class 1 and tuples with low values to class 0
		
//		double sumOfFeatures = 0;
//		for (double featVal : record.getFeatures().values()) {
//			sumOfFeatures += featVal;
//		}
//		double mean = sumOfFeatures / record.getFeatures().size();
//		
//		if (mean > 0.5) {
//			return 1;
//		} else {
//			return 0;
//		}
		
		Random r = new Random();
		return r.nextInt(2);
		
	}

}
